const { Cars } = require("../models")

module.exports = {
    create(createArgs) {
      return Cars.create(createArgs);
    },
  
    update(id, updateArgs) {
      return Cars.update(updateArgs, {
        where: {
          id,
        },
      });
    },

    deleted(id, updateArgs) {
      return Cars.update(updateArgs, {
        where: {
          id,
        },
      });
    },
  
    find(id) {
      return Cars.findByPk(id);
    },
  
    findOne(key) {
      return Cars.findOne(key);
    },

    findAll(args) {
      return Cars.findAll(args);
    },
  
    getTotalCars(args) {
      return Cars.count(args);
    },
  };
  
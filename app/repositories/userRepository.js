const { Users } = require("../models")

module.exports = {
  create(createArgs) {
    return Users.create(createArgs);
  },
  
  update(id, updatedData) {
    return Users.update(updatedData, {
      where: {
        id,
      },
    });
  },

  // delete(id) {
  //   return Users.destroy(id);
  // },

  find(id) {
    return Users.findByPk(id);
  },

  // findAll() {
  //   return Users.findAll();
  // },

  // getTotalUsers() {
  //   return Users.count();
  // },


  findOne(key) {
    return Users.findOne(key);
  },

};

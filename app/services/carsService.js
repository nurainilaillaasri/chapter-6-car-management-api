const carRepository = require("../repositories/carRepository")

module.exports = {
  create(requestBody) {
    return carRepository.create(requestBody);
  },

  update(id, requestBody) {
    return carRepository.update(id, requestBody);
  },

  deleted(id, requestBody) {
    return carRepository.deleted(id, requestBody);
  },

  async list(args) {
    try {
      const cars = await carRepository.findAll(args);
      const carsCount = await carRepository.getTotalCars(args);

      return {
        data: cars,
        count: carsCount,
      };
    } catch (err) {
      throw err;
    }
  },

  get(id) {
    return carRepository.find(id);
  },

  getOne(key) {
    return carRepository.findOne(key);
  },
};
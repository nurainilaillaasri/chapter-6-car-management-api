/**
 * @file contains entry point of controllers module
 * @author Fikri Rahmat Nurhidayat
 */

const api = require("./api");
const main = require("./main");
// const authController = require("./authController")

module.exports = {
  api,
  main,
  // authController,
};

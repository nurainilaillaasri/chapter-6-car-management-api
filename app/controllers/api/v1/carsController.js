const carsService = require("../../../services/carsService");

module.exports = {
  list(req, res) {
    carsService
      .list({
        where: {isDeleted: false},
      })
      .then((data, count) => {
        res.status(200).json({
          status: "OK",
          data: {
            cars: data
          },
          meta: { total: count },
        });
      })
      .catch((err) => {
        res.status(400).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  create(req, res) {
    req.body.createdBy = req.user.user_email;
    carsService
      .create(req.body)
      .then((cars) => {
        res.status(201).json({
          status: "OK",
          data: cars,
        });
      })
      .catch((err) => {
        res.status(401).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  show(req, res) {
    carsService
      .get(req.params.id)
      .then((post) => {
        res.status(200).json({
          status: "OK",
          data: post,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  update(req, res) {
    req.body.updatedBy = req.user.user_email;
    carsService
      .update(req.params.id, req.body)
      .then(() => {
        res.status(200).json({
          status: "OK",
          message: "Data berhasil diperbarui",
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  deleted(req, res) {
    carsService
      .deleted(req.params.id, { isDeleted: true, deletedBy: req.user.user_email })
      .then(() => {
        res.status(200).json({
          deletedBy: req.user.user_email,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  // destroy(req, res) {
  //   carsService.deleteCar(req.cars)
  //     .then(() => {
  //       res.status(204).end();
  //     })
  //     .catch((err) => {
  //       res.status(422).json({
  //         status: "FAIL",
  //         message: err.message,
  //       });
  //     });
  // },

  // deleted(req, res) {
  //   const cars = req.cars;
  //   carsService
  //     .update(req.params.id, {
  //       car_name,
  //       rent_cost,
  //       type,
  //       // image,
  //       createdBy,
  //       updatedBy,
  //       deletedBy: req.user.id
  //     })
  //     .then(() => {
  //       res.status(200).json({
  //         status: "OK",
  //         data: cars,
  //       });
  //     })
  //     .catch((err) => {
  //       res.status(422).json({
  //         status: "FAIL",
  //         message: err.message,
  //       });
  //     });
  // },

  // setCars(req, res, next) {
  //   carsService.get(req.params.id)
  //     .then((cars) => {
  //       if (!cars) {
  //         res.status(404).json({
  //           status: "FAIL",
  //           message: "Post not found!",
  //         });

  //         return;
  //       }

  //       req.cars = cars;
  //       next()
  //     })
  //     .catch((err) => {
  //       res.status(404).json({
  //         status: "FAIL",
  //         message: "Post not found!",
  //       });
  //     });
  // },
};

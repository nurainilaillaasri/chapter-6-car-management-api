# Binar Car Management API

Repository Car Management API ini menggunakan authentication dan authorization

## Getting Started

Untuk mulai install semua node dengan:

```sh
nmp install
```

Edit file [`config/database.js`] sesuai dengan database dan password yang digunakan:
```sh
DB_USERNAME = ,
DB_PASSWORD = ,
```

Buat file .env yang berisi sama dengan [`.env-example`], sesuaikan isinya 
```sh
SUPERADMIN_PASSWORD = 
JWT_PRIVATE_KEY = 
```

Untuk menjalankan:

```sh
nmp run start
```

## Database Management

Di dalam repository ini sudah terdapat beberapa script yang dapat digunakan dalam memanage database, yaitu:

- `yarn db:create` digunakan untuk membuat database
- `yarn db:drop` digunakan untuk menghapus database
- `yarn db:migrate` digunakan untuk menjalankan database migration
- `yarn db:seed` digunakan untuk melakukan seeding
- `yarn db:rollback` digunakan untuk membatalkan migrasi terakhir

